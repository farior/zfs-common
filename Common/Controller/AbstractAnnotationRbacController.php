<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 15:20
 */

namespace ZFS\Common\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

abstract class AbstractAnnotationRbacController extends AbstractActionController
{
    public function onDispatch(MvcEvent $e)
    {
        $action = $this->params('action')."Action";

        $reflection = new \ReflectionClass($this);
        $method = $reflection->getMethod($action);

        $phpDoc = $method->getDocComment();

        preg_match_all('#@permission\s(.*?)\n#s', $phpDoc, $permissionAnnotation);

        if (!empty($permissionAnnotation[1])) {
            $permission = $permissionAnnotation[1][0];

            if (!$this->isGranted($permission)) {
                return $this->redirect()->toRoute('home');
            }
        }

        return parent::onDispatch($e);
    }
}
