<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 15:20
 */

namespace ZFS\Common\Controller;

use Zend\EventManager\EventManagerInterface;

abstract class AbstractManagementController extends AbstractAnnotationRbacController
{
    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);

        $this->events->addIdentifiers('ZFS\Dashboard\Event');

        return $this;
    }
}
