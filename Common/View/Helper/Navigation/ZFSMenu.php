<?php
/**
 * User: dev
 * Date: 25.02.15 13:30
 */

namespace ZFS\Common\View\Helper\Navigation;

use Zend\View\Helper\Navigation\Menu;
use Zend\Navigation\Page\AbstractPage;
use Zend\EventManager\EventManagerAwareInterface;

class ZFSMenu extends Menu implements EventManagerAwareInterface
{
    /**
     * Returns an HTML string containing an 'a' element for the given page if
     * the page's href is not empty, and a 'span' element if it is empty
     *
     * Overrides {@link AbstractHelper::htmlify()}.
     *
     * @param  AbstractPage $page               page to generate HTML for
     * @param  bool         $escapeLabel        Whether or not to escape the label
     * @param  bool         $addClassToListItem Whether or not to add the page class to the list item
     * @return string
     */
    public function htmlify(AbstractPage $page, $escapeLabel = true, $addClassToListItem = false)
    {
        if (isset($page->event)) {
            $markup = '';

            $this->getEventManager()
                ->addIdentifiers('ZFS\Event\Navigation')
                ->trigger($page->event, $this, array('page' => $page), function ($html) use (&$markup) {
                    $markup = $html;
                });

            return $markup;
        }

        return parent::htmlify($page, $escapeLabel, $addClassToListItem);
    }

    /**
     * Determines whether a page should be accepted when iterating
     *
     * Default listener may be 'overridden' by attaching listener to 'isAllowed'
     * method. Listener must be 'short circuited' if overriding default ACL
     * listener.
     *
     * Rules:
     * - If a page is not visible it is not accepted, unless RenderInvisible has
     *   been set to true
     * - If $useAcl is true (default is true):
     *      - Page is accepted if listener returns true, otherwise false
     * - If page is accepted and $recursive is true, the page
     *   will not be accepted if it is the descendant of a non-accepted page
     *
     * @param   AbstractPage    $page       page to check
     * @param   bool            $recursive  [optional] if true, page will not be
     *                                      accepted if it is the descendant of
     *                                      a page that is not accepted. Default
     *                                      is true
     *
     * @return  bool                        Whether page should be accepted
     */
    public function accept(AbstractPage $page, $recursive = true)
    {
        $accept = true;

        if (!$page->isVisible(false) && !$this->getRenderInvisible()) {
            $accept = false;
        } else {
            $accept = $this->view->isGranted($page->getPermission());
        }

        if ($accept && $recursive) {
            $parent = $page->getParent();

            if ($parent instanceof AbstractPage) {
                $accept = $this->accept($parent, true);
            }
        }

        return $accept;
    }
}
