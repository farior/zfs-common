<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.04.15
 * Time: 15:02
 */

namespace ZFS\Common\Controller\Plugin;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\FlashMessenger as ZendFlashMessenger;

class FlashMessenger extends ZendFlashMessenger
{
    const MESSAGE_CONFIG_KEY = 'flash_messages';

    /**
     * Add a message
     *
     * @param  string         $message
     * @param  array          $data
     * @return FlashMessenger Provides a fluent interface
     */
    public function addMessage($message, $data = array())
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        $config = $controller->getServiceLocator()->get('Config');

        if (isset($config[self::MESSAGE_CONFIG_KEY][$message])) {
            $message = $config[self::MESSAGE_CONFIG_KEY][$message];
        }

        foreach ($data as $key => $value) {
            $message = str_replace("%" . $key . "%", $value, $message);
        }

        return parent::addMessage($message);
    }

    /**
     * Add a message with "info" type
     *
     * @param  string         $message
     * @param  array          $data
     * @return FlashMessenger
     */
    public function addInfoMessage($message, $data = array())
    {
        $namespace = $this->getNamespace();
        $this->setNamespace(self::NAMESPACE_INFO);
        $this->addMessage($message, $data);
        $this->setNamespace($namespace);

        return $this;
    }

    /**
     * Add a message with "success" type
     *
     * @param  string         $message
     * @param  array          $data
     * @return FlashMessenger
     */
    public function addSuccessMessage($message, $data = array())
    {
        $namespace = $this->getNamespace();
        $this->setNamespace(self::NAMESPACE_SUCCESS);
        $this->addMessage($message, $data);
        $this->setNamespace($namespace);

        return $this;
    }

    /**
     * Add a message with "warning" type
     *
     * @param string        $message
     * @param  array          $data
     * @return FlashMessenger
     */
    public function addWarningMessage($message, $data = array())
    {
        $namespace = $this->getNamespace();
        $this->setNamespace(self::NAMESPACE_WARNING);
        $this->addMessage($message, $data);
        $this->setNamespace($namespace);

        return $this;
    }

    /**
     * Add a message with "error" type
     *
     * @param  string         $message
     * @param  array          $data
     * @return FlashMessenger
     */
    public function addErrorMessage($message, $data = array())
    {
        $namespace = $this->getNamespace();
        $this->setNamespace(self::NAMESPACE_ERROR);
        $this->addMessage($message, $data);
        $this->setNamespace($namespace);

        return $this;
    }
}
