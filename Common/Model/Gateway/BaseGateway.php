<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.01.15
 * Time: 16:08
 */

namespace ZFS\Common\Model\Gateway;

use ZFS\DomainModel\Gateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZFS\DomainModel\Object\ObjectInterface;

class BaseGateway extends TableGateway implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @param array $where
     * @return ObjectInterface
     *
     * @codeCoverageIgnore
     */
    public function selectOne($where)
    {
        return $this->select($where)->current();
    }
}
