<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 13:10
 */

return array(
    'navigation_helpers' => array (
        'invokables' => array(
            'Menu' => 'ZFS\Common\View\Helper\Navigation\ZFSMenu'
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'flashMessenger' => 'ZFS\Common\Controller\Plugin\FlashMessenger'
        )
    ),
);
